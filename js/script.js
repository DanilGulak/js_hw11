let activeBtn = document.querySelectorAll('.btn');

document.body.addEventListener('keyup', (event) => {
    activeBtn.forEach( (element) => {
        if (element.textContent === event.key || element.textContent.toLowerCase() === event.key) {
            element.style.backgroundColor = 'blue';
        } else {
            element.style.backgroundColor = 'black';
        }
    })
})